# @uhuru/node-red-contrib-omron-2jcie-bu-ble

## Requirements

- Python 3.6 or later
- pip 18.1 or later

## Installation

To install the stable version use the Menu - Manage palette - Install option and search for node-red-contrib-omron-2jcie-bu-ble, or run the following command in your Node-RED user directory, typically ~/.node-red

```
cd ~/.node-red
npm install @uhuru/node-red-contrib-omron-2jcie-bu-ble
```

If you use Node-RED on Linux environemnt, you need to set up the privilege.
Please execute following commands.

```
sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

See below for more information.
https://www.npmjs.com/package/noble#running-on-linux

## Usage

### Input

#### msg object

For this node, the input msg object is just a trigger.

#### sensor data

This node reads sensor data from ble advertisment.
A user must set the Bluetooth Addresses (e.q. 'A1:B2:C3:D4:E5:F6,F6:E5:D4:C3:B2:A1').

### Output

This node outputs msg objects including sensor data and serial number.
The format of the message is below.

#### Output

```
msg: {
    payload: {
        {
            "A1:B2:C3:D4:E5:F6": {
                "sensorData": {
                    "temperature": 29.09,
                    "relativeHumidity": 74.48,
                    "ambientLight": 82,
                    "barometricPressure": 1007.5790000000001,
                    "soundNoise": 40.480000000000004,
                    "etvoc": 20,
                    "eco2": 534
                },
                "serialNumber": {serial number}
            },
            "F6:E5:D4:C3:B2:A1": {
                "sensorData": {
                    "temperature": 24.8,
                    "relativeHumidity": 75.4,
                    "ambientLight": 78,
                    "barometricPressure": 1007.738,
                    "soundNoise": 41.480000000000004,
                    "etvoc": 6,
                    "eco2": 441
                },
                "serialNumber": {serial number}
            }
        }
    }
}
```

## Copyright and license

Copyright Uhuru Coproration under the Apache 2.0 license.