import sys
from bluepy import btle

args = sys.argv
timeout = 1000
macAddresses = []
errors = []
KEY_NAME = 'Short Local Name'
SHORT_LOCAL_NAME = 'Rbt'
LATEST_DATA_SERVICE_UUID = 'ab705010-0a3a-11e8-ba89-0ed5f89f718b'
LATEST_SENSING_DATA_UUID = 'ab705012-0a3a-11e8-ba89-0ed5f89f718b'
LATEST_SENSING_FLAG_UUID = 'ab705014-0a3a-11e8-ba89-0ed5f89f718b'
DEVICE_INFOMATION_SERVICE_UUID = '0000180a-0000-1000-8000-00805f9b34fb'
SERIAL_NUMBER_STRING_UUID = '00002a25-0000-1000-8000-00805f9b34fb'

def is_specified_timeout():
  if 2 <= len(args):
    if (args[1].isdecimal()) and (0 < int(args[1]) <= 10000):
      return True
  return False

# デバイスのスキャン
scanner = btle.Scanner(0)
if is_specified_timeout():
  timeout = int(args[1])
devices = scanner.scan(timeout/1000)

for device in devices:
  for (adTypeCode, description, valueText) in device.getScanData():
    if (description == KEY_NAME) and (valueText == SHORT_LOCAL_NAME):
      macAddresses += [device.addr] 

# SerialNumbers
serialNumbers = '{'

for macAddress in macAddresses:

  try:
    # Peripheralに接続
    peripheral = btle.Peripheral(macAddress, btle.ADDR_TYPE_RANDOM)

    # Device Serial Numberの取得
    serialNumber = peripheral.getServiceByUUID(DEVICE_INFOMATION_SERVICE_UUID).getCharacteristics(SERIAL_NUMBER_STRING_UUID)[0].read()

    serialNumbers += f'"{macAddress}":' + f'"{serialNumber.hex()}"' + ','

    peripheral.disconnect()
  
  except Exception as e:
    errors += [e]

if ((len(macAddresses) > 0) and (serialNumbers != '{')):
  serialNumbers = serialNumbers[:-1]

serialNumbers += '}'

# 標準出力
print(serialNumbers)
# 標準エラー出力
print(errors, file=sys.stderr)