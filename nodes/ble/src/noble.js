const noble = require('@abandonware/noble');
const AdvertisingInterval = 100  // Omronセンサーアドバタイズパケット送信インターバル(ms)
let isScanning = false;

async function sleep(ms) {
  return new Promise(r => setTimeout(r, ms));
}

async function doScanning() {
  if (!isScanning) {
    isScanning = true;
    // アドバタイズのスキャンを開始
    await noble.startScanningAsync([], true);
    // 2周期分データを待つ
    await sleep(AdvertisingInterval*2);
    // アドバタイズのスキャンを停止
    await noble.stopScanningAsync();
    isScanning = false;
  }
}

noble.on('stateChange', async (state) => {
  if (state === 'poweredOn') {
    await doScanning();
  }
});

noble.on('discover', async (peripheral) => {
  if (peripheral.advertisement.localName === 'Rbt') {
    let data = {
      'address': peripheral.address,
      'sensorData': {
        'temperature':parseFloat((peripheral.advertisement.manufacturerData.readInt16LE(9 - 5) * 0.01).toFixed(2)),
          'relativeHumidity':parseFloat((peripheral.advertisement.manufacturerData.readInt16LE(11 - 5) * 0.01).toFixed(2)),
          'ambientLight':peripheral.advertisement.manufacturerData.readInt16LE(13 - 5),
          'barometricPressure':parseFloat((peripheral.advertisement.manufacturerData.readInt32LE(15 - 5) * 0.001).toFixed(3)),
          'soundNoise':parseFloat((peripheral.advertisement.manufacturerData.readInt16LE(19 - 5) * 0.01).toFixed(2)),
          'etvoc':peripheral.advertisement.manufacturerData.readInt16LE(21 - 5),
          'eco2':peripheral.advertisement.manufacturerData.readInt16LE(23 - 5)
      }
    };
    process.send(data);
  }
});

setInterval(async function(){
  await doScanning();
}, AdvertisingInterval*10);

