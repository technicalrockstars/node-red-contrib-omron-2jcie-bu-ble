const childProcess = require('child_process');

module.exports = function(RED) {
  function Omron2jcieBuBle(config) {

    // 初期化処理
    RED.nodes.createNode(this,config);
    const pythonScriptName = 'getSerialNumber.py';
    let sensorData = new Map();
    let node = this;
    const expirationTime = 10000;       // 古いセンサーデータの削除チェックサイクル(ms)

    try {
      node.devices = (config.devices.toLowerCase()).split(',');
    } catch(e) {
      node.error(`Invalid input : ${e}`);
    }
      node.time_out = config.time_out;
  
    // デバイスに接続しシリアル番号を取得
    let addressVsSerialNumber;
    try {
      addressVsSerialNumber = childProcess.execSync(`sudo python3 ${__dirname}/${pythonScriptName} ${node.time_out}`).toString();
    } catch(e) {
      node.log(`Sensor connecting error: ${e}`);
    }
    node.log(`target devices: ${addressVsSerialNumber}`);
    addressVsSerialNumber = JSON.parse(addressVsSerialNumber);

    // child processでアドバタイズの受信を開始
    let nobleProc;
    try {
      nobleProc = childProcess.fork(`${__dirname}/noble`);
      nobleProc.on('message', (data) => {
        sensorData.set(
          `${data.address}`, {
            'sensorData': data.sensorData,
            'serialNumber' : (addressVsSerialNumber[data.address] === undefined) ? null : addressVsSerialNumber[data.address],
            'timestamp' : Date.now()
          }
        );
      });
    } catch(e) {
      nobleProc.kill();
      node.log(`advertisement receiving error: ${e}`);
    }

    node.on('input', function(msg) {
      // 古いセンサーデータの削除
      deleteOldSensorData();
      msg.payload = {};
      for(let device of node.devices) {
        msg.payload[device] = (sensorData.get(device) === undefined) ? {'sensorData':null, 'serialNumber':null} : sensorData.get(device);
        delete msg.payload[device].timestamp;
      }
      node.send(msg);

    });

    node.on('close', function() {
      nobleProc.kill();
      node.log("closed");
    });

    // 定期的に古いセンサーデータを削除する処理   
    var deleteOldSensorData = function(){
      for(let device of node.devices) {
        if((sensorData.size === 0) || (sensorData.get(device) === undefined)) {
          continue;
        }
        if((Date.now() - (sensorData.get(device)).timestamp) > expirationTime) {
          let serialNumber = (addressVsSerialNumber[device] === undefined) ? null : addressVsSerialNumber[device];
          sensorData.set(
            `${device}`, {
              'sensorData': null,
              'serialNumber' : serialNumber,
              'timestamp' : Date.now()
            }
          );          
          node.log(`old sensor data deleted: ${device}`);
        }
      }
    };

  }
  RED.nodes.registerType("2jcie-bu-ble",Omron2jcieBuBle);
}